import { Route, Switch } from 'react-router-dom';
import NotFound from '../../pages/NotFound';
import React from 'react';
import HeroesPage from '../../pages/HeroesPage';
import HeroDetailPage from '../../pages/HeroDetailPage';

const App = () => {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={HeroesPage} />
        <Route exact path="/hero/:id" component={HeroDetailPage} />
        <Route component={NotFound} />
      </Switch>
    </div>
  );
};

export default App;
