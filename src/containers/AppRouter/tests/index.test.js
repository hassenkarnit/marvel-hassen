import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import { configureStore } from '../../../_redux/store';
import { Provider } from 'react-redux';
import { MemoryRouter, Route } from 'react-router-dom';

describe('pages | AppRouter ', () => {
  it('should render App Router', () => {
    const store = configureStore();
    const { container } = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/1']}>
          <Route path="/:charachterId">
            <Component />
          </Route>
        </MemoryRouter>
      </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});
