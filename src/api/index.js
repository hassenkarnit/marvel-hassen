const axios = require('axios');
var crypto = require('crypto');
const baseUrl = 'http://gateway.marvel.com:80';
const uri = '/v1/public/characters';
const charactersUrl = baseUrl + uri;
const timestamp = [Math.round(+new Date() / 1000)];
const privateApi = 'b00f9a6c1f37f7f5a5612f1571e335eecdfbd4ce';
const publicApi = 'bca905dbfe9279c6324cb1229f963f6d';
const concatenatedString = timestamp.concat(privateApi, publicApi).join('');
const hash = crypto.createHash('md5').update(`${concatenatedString}`).digest('hex');
export const getHeroes = () =>
  axios.get(charactersUrl, {
    params: {
      ts: timestamp,
      apikey: publicApi,
      hash,
    },
  });
export const getOneHero = characterId =>
  axios.get('http://gateway.marvel.com/v1/public/characters/' + characterId, {
    params: {
      ts: timestamp,
      apikey: publicApi,
      hash,
    },
  });
