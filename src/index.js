import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';
import store from './_redux/store';
import Root from './containers/AppRouter';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="/" component={Root} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root'),
);
