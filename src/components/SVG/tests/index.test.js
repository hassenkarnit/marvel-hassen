import React from 'react';
import { render } from '@testing-library/react';
import Component from '../NotFoundSVG';

describe.only('components | SVG | index', () => {
  it('should render component', () => {
    const { container } = render(<Component />);
    expect(container).toMatchSnapshot();
  });
});
