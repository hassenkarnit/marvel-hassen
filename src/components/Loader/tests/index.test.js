import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';

describe.only('components | Loader | index', () => {
  it('should render component', () => {
    const { container } = render(<Component />);
    expect(container).toMatchSnapshot();
  });
});
