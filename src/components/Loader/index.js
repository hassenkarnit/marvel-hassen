import { CircularProgress } from '@material-ui/core';
import React from 'react';

export const LoaderComponent = () => {
  return <CircularProgress />;
};

export default LoaderComponent;
