// import { useEffect, useState } from 'react';
// import loadImages from 'image-promise';

// export const useAsyncImage = (image, fallback) => {
//   const [src, setSrc] = useState('/assets/images/logo1.png');
//   const [loading, setLoadingImage] = useState(true);

//   useEffect(() => {
//     setLoadingImage(true);
//     loadImages(image)
//       .then(() => {
//         setSrc(image);
//         setLoadingImage(false);
//       })
//       .catch(() => {
//         loadImages(fallback)
//           .then(() => {
//             setSrc(fallback);
//             setLoadingImage(false);
//           })
//           .catch(() => {
//             setSrc('/assets/images/logo1.png');
//             setLoadingImage(false);
//           });
//       });
//   }, [image, fallback]);

//   return { src, loading };
// };
