import React from 'react';
import PropTypes from 'prop-types';
// import LoaderComponent from '../../components/Loader';
// import { useAsyncImage } from './hooks';

const ImageAsyncComponent = ({
  image,
  // fallback = '/assets/images/logo1.png'
}) => {
  // const { loading, src } = useAsyncImage(image, fallback);
  return (
    <div className="position-relative">
      {/* {loading && (
        <div style={{ position: 'absolute', zIndex: '99' }}>
          <LoaderComponent />
        </div>
      )} */}

      {/* <div style={loading ? { filter: 'blur(7px)' } : undefined}> */}
      <div>
        <div
          style={{
            backgroundImage: `url(${image})`,
            backgroundSize: 'cover',
            width: '7em',
            height: '7em',
          }}
        />
      </div>
    </div>
  );
};

ImageAsyncComponent.propTypes = {
  image: PropTypes.string,
  fallback: PropTypes.string,
};
export default ImageAsyncComponent;
