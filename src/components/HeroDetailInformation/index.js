import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import PropTypes from 'prop-types';

const HeroDetailInformation = ({ comics, series, title }) => {
  return (
    <Grid container direction="column">
      <Grid>
        <Typography variant="h5">{title}</Typography>
      </Grid>
      <Grid>
        <Typography variant="h6">comics :</Typography>
        {comics?.map((comic, index) => (
          <Grid key={index}>
            <Typography>{comic.name}</Typography>
          </Grid>
        ))}
      </Grid>
      <Grid>
        <Typography variant="h6">series :</Typography>
        {series?.map((hero, index) => (
          <Grid key={index}>
            <Typography>{hero.name}</Typography>
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

HeroDetailInformation.propTypes = {
  comics: PropTypes.array,
  series: PropTypes.array,
  title: PropTypes.string,
};

export default HeroDetailInformation;
