import React from 'react';
import PropTypes from 'prop-types';
import { Grid, makeStyles, Typography } from '@material-ui/core';
import ImageAsyncComponent from '../ImageAsyncComponent';
import LinkIcon from '@material-ui/icons/Link';
import { Link } from 'react-router-dom';
import useMobileDetect from 'use-mobile-detect-hook';

const useStyles = makeStyles(() => ({
  cardContainer: {
    margin: '10px',
    border: 'solid',
    borderWidth: '1px',
    minHeight: '320px',
  },
}));
const HeroCard = ({ hero }) => {
  const isMobile = useMobileDetect().isMobile();
  const classes = useStyles();
  return (
    <Grid
      container
      item
      xs={isMobile ? 12 : 3}
      direction="column"
      justify="center"
      alignItems="center"
      className={classes.cardContainer}
    >
      <Grid item>
        <Link to={`/hero/${hero.id}`}>
          <ImageAsyncComponent image={hero?.thumbnail?.path + '.' + hero?.thumbnail?.extension} />
        </Link>
      </Grid>
      <Grid item>
        <Typography style={{ color: 'black' }}>{hero?.name}</Typography>
      </Grid>
      <Grid container>
        {hero?.urls?.map((url, index) => (
          <Grid container item key={index} style={{ width: '33%' }}>
            <Grid item container alignItems="center" style={{ marginLeft: '10px' }} component="a" href={url?.url}>
              <Grid item>
                <LinkIcon style={{ marginBottom: '-2.5px', fontSize: '20px' }} />
              </Grid>
              <Grid item>
                <Typography style={{ fontSize: '13px' }}>{url.type}</Typography>
              </Grid>
            </Grid>
          </Grid>
        ))}
      </Grid>
    </Grid>
  );
};

export default HeroCard;

HeroCard.propTypes = {
  hero: PropTypes.object,
};
