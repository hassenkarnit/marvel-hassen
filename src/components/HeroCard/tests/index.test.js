import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import { MemoryRouter, Route } from 'react-router-dom';

describe.only('components | HeroCard | index', () => {
  it('should render component', () => {
    const hero = {};
    const { container } = render(
      <MemoryRouter initialEntries={['/1']}>
        <Route path="/:charachterId">
          <Component hero={hero} />
        </Route>
      </MemoryRouter>,
    );
    expect(container).toMatchSnapshot();
  });
});
