import { startLoading, stopLoading } from '../Loading';
import { getHeroes, getOneHero } from '../../../api';
import { FETCH_HEROES, FETCH_ONE_HERO } from '../../constants/types';

export const fetchHeroes = () => async dispatch => {
  dispatch(startLoading());
  const response = await getHeroes();
  dispatch({ type: FETCH_HEROES, payload: response.data.data.results });
  dispatch(stopLoading());
};
export const fetchOneHero = id => async dispatch => {
  dispatch(startLoading());
  const response = await getOneHero(id);
  // const oneHeroData = {
  //   image: 'https://images-na.ssl-images-amazon.com/images/I/61MlZAUjV2L._SX325_BO1,204,203,200_.jpg',
  //   name: 'XMAN',
  //   comics: [
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //   ],
  //   series: [
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //     '2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman2016 serie xman',
  //   ],
  // };
  dispatch({ type: FETCH_ONE_HERO, payload: response.data.data.results[0] });
  dispatch(stopLoading());
};
