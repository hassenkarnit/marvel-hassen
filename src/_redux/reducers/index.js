import { combineReducers } from 'redux';
import loading from './Loading';
import heroesReducer from './Heroes';

export default combineReducers({
  loading,
  heroesReducer,
});
