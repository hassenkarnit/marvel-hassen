import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import { MemoryRouter, Route } from 'react-router-dom';

describe('pages | NotFOundPage ', () => {
  it('should render NotFOundPage', () => {
    const { container } = render(
      <MemoryRouter initialEntries={['/1']}>
        <Route path="/:charachterId">
          <Component />
        </Route>
      </MemoryRouter>,
    );
    expect(container).toMatchSnapshot();
  });
});
