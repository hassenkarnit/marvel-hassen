import React from 'react';
import { render } from '@testing-library/react';
import Component from '..';
import { configureStore } from '../../../_redux/store';
import { Provider } from 'react-redux';

describe('pages | HeroesPage ', () => {
  it('should render HeroesPage', () => {
    const store = configureStore();
    const { container } = render(
      <Provider store={store}>
        <Component />
      </Provider>,
    );
    expect(container).toMatchSnapshot();
  });
});
