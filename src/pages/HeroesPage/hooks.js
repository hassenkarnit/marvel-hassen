import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { fetchHeroes } from '../../_redux/actions/Heroes';

export const useHeroesPage = () => {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchHeroes());
  }, []);
  const { heroes } = useSelector(({ heroesReducer }) => heroesReducer);

  return { heroes };
};
