import { Grid, makeStyles, Typography } from '@material-ui/core';
import React from 'react';
import { useHeroesPage } from './hooks';
import HeroCard from '../../components/HeroCard';

const useStyles = makeStyles(() => ({
  rootContainer: {
    fontSize: '2em',
    margin: '30px',
  },
  title: {
    margin: '30px',
  },
}));

const HeroesPage = () => {
  const { heroes } = useHeroesPage();
  const classes = useStyles();
  return (
    <Grid container justify="center" className={classes.rootContainer}>
      <Typography className={classes.title} variant="h5">
        List of marvels heroes
      </Typography>
      <Grid container justify="space-around">
        {heroes.map((hero, index) => (
          <HeroCard key={index} hero={hero} />
        ))}
      </Grid>
    </Grid>
  );
};

export default HeroesPage;
