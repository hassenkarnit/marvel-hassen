import { Grid, Typography } from '@material-ui/core';
import React from 'react';
import { Link } from 'react-router-dom';
import HeroDetailInformation from '../../components/HeroDetailInformation';
import ImageAsyncComponent from '../../components/ImageAsyncComponent';
import { useHeroDetailPage } from './hooks';

const HeroDetailPage = () => {
  const { hero } = useHeroDetailPage();
  console.log('hero', hero);

  return (
    <Grid container justify="center">
      <Typography variant="h5">Hero Detail Page</Typography>
      {hero && (
        <>
          <Grid item container justify="center">
            <Grid container justify="center" item xs={3}>
              <Grid>
                <ImageAsyncComponent image={hero.thumbnail.path + '.' + hero.thumbnail.extension} />
              </Grid>
            </Grid>
            <Grid item xs={9}>
              <HeroDetailInformation comics={hero.comics.items} series={hero.series.items} title={hero.name} />
            </Grid>
          </Grid>
        </>
      )}
      <Link to="/">
        {' '}
        <Typography>return to list of Heroes</Typography>
      </Link>
    </Grid>
  );
};

export default HeroDetailPage;
