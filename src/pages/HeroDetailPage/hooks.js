import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch } from 'react-router-dom';
import { fetchOneHero } from '../../_redux/actions/Heroes';

export const useHeroDetailPage = () => {
  const dispatch = useDispatch();
  const { params } = useRouteMatch();
  console.log(params);

  useEffect(() => {
    dispatch(fetchOneHero(params.id));
  }, [dispatch]);
  const { hero } = useSelector(({ heroesReducer }) => heroesReducer);

  return { hero };
};
