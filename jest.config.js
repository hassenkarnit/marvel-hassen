module.exports = {
  verbose: true,

  collectCoverageFrom: [
    '!src/proxy/**',
    '!src/mock/**',
    'src/**/*.js',
    '!src/**/*.test.{js,jsx}',
    '!**/*e2e.js',
    '!**/web/tests/**',
  ],
  moduleNameMapper: {
    '.*\\.(css|less|styl|scss|sass)$': '<rootDir>/internals/cssModule.js',
    '.*\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/internals/image.js',
  },
  setupFilesAfterEnv: ['<rootDir>/jest-setup.js'],
  testRegex: 'tests/.*\\.test\\.js$',
};
